from django.conf.urls import url
from message.views import IndexView
from message import views
urlpatterns = [
    url(r'^$', IndexView.as_view()),
    url(r'^board$', views.board),
    url(r'^get_tips$', views.get_tips),
    url(r'^get_device$', views.get_device),
    url(r'^register/$', views.register),
    url(r'^manager/$', views.manager),
]























