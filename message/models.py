from django.db import models


# Create your models here.
class Device(models.Model):
    mac = models.CharField(max_length=64)                # mac地址
    name = models.CharField(max_length=64)               # 设备名称
    type = models.CharField(max_length=64)               # 设备的类型
    Firmware = models.CharField(max_length=64)           # 设备固件
    note = models.CharField(max_length=64)               # 评论
    active_time = models.DateField()                     # 激活时间
    version = models.CharField(max_length=64)            # 版本号
    online_time = models.DateField()                     # 最后一次上线时间
    offline_time = models.DateField()                    # 最后一次下线时间
    user_name = models.CharField(max_length=64)          # 用户名


class Team(models.Model):
    name = models.CharField(max_length=64)               # 设备组名称
    user_name = models.CharField(max_length=64)          # 用户名
    device = models.ForeignKey(Device)                   # 和设备间建立外键关系













