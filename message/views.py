from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
# Create your views here.
from message.models import Device
from django.views.generic import View
from django.core.paginator import Paginator
from django.contrib.auth.models import User


class IndexView(View):

    def get(self, request):

        context = {}
        page = request.GET.get('page')

        if not page:
            page = 1
        page = str(page)
        page = int(page)
        try:
            devices = Device.objects.all()
            for device in devices:

                if device.offline_time > device.online_time:

                    device.status = 'offline'
                else:
                    device.status = 'online'
        except Exception as e:
            return HttpResponse('查询数据库失败')

        paginator = Paginator(devices, 10)
        devices = paginator.page(page)
        num_pages = paginator.num_pages
        if num_pages < 5:
            pages = range(1, num_pages+1)
        elif page <= 3:
            pages = range(1, 6)
        elif num_pages - page <= 2:
            pages = range(num_pages-4, num_pages+1)
        else:
            pages = range(page-2, page+3)
        context['devices'] = devices
        context['pages'] = pages
        alist = [10, 20]
        status_list = ['全部', '在线', '离线']
        context['alist'] = alist
        context['status_list'] = status_list
        return render(request, 'message/device.html', context)

    def post(self, request):

        context = {}
        status = request.POST.get('Status')
        mac = request.POST.get('Mac')
        type = request.POST.get('type')
        version = request.POST.get('version')
        amount = request.POST.get('amount')
        page = request.POST.get('page')

        if not page:
            page = 1
        else:
            context['page'] = page

        try:
            devices = Device.objects.all()
            if type:
                devices = devices.filter(type=type)
                context['type'] = type
            if mac:
                devices = devices.filter(mac__contains=mac)
                context['mac'] = mac
            if version:
                devices = devices.filter(version=version)
                context['version'] = version
        except Exception as e:
            return HttpResponse('查询数据库失败')

        if status != "全部":
            context['status'] = status
            online_devices = []
            offline_devices = []

            for device in devices:
                if device.offline_time > device.online_time:
                    device.status = 'offline'
                    offline_devices.append(device)
                else:
                    device.status = 'online'
                    online_devices.append(device)

            if status == '在线':
                devices = online_devices
                context['status'] = status
            else:
                devices = offline_devices
        else:
            for device in devices:
                if device.offline_time > device.online_time:
                    device.status = 'offline'
                else:
                    device.status = 'online'
        amount = int(amount)
        page = int(page)
        paginator = Paginator(devices, amount)
        if page > paginator.num_pages:
            page = 1
        devices = paginator.page(page)
        num_pages = paginator.num_pages
        if num_pages < 5:
            pages = range(1, num_pages+1)
        elif page <= 3:
            pages = range(1, 6)
        elif num_pages - page <= 2:
            pages = range(num_pages-4, num_pages+1)
        else:
            pages = range(page-2, page+3)
        context['devices'] = devices
        context['pages'] = pages

        return render(request, 'message/device.html', context)


def get_tips(request):

    json_str = request.body.decode()
    content = {}

    try:
        str_list = json_str.split("=")
        data = str_list[1]
    except Exception as e:
        return JsonResponse({'res': 0, 'errmsg': '请求数据不对'})

    try:
        devices = Device.objects.filter(mac__contains=data)
    except Exception as e:
        return JsonResponse({'res': 1, 'errmsg': '查询不到相应的参数'})

    for device in devices:
        content[device.mac] = device.mac
    return JsonResponse({'res': 2, 'errmsg': '查询成功', 'content': content})


def get_device(request):

    mac = request.GET.get('n')
    try:
        devices = Device.objects.filter(mac__contains=mac)

    except Exception as e:
        return HttpResponse('查询数据库失败')

    for device in devices:
        if device.offline_time > device.online_time:
            device.status = 'offline'
        else:
            device.status = 'online'

    total = len(devices)
    context = {
        'devices': devices,
        'total': total
    }
    return render(request, 'message/device.html', context)


def board(request):
    try:
        devices = Device.objects.all()
        on_line_count = 0
        off_line_count = 0
        total = len(devices)
        for device in devices:
            if device.offline_time > device.online_time:
                off_line_count += 1
            else:
                on_line_count += 1
    except Exception as e:
        return HttpResponse('查询数据库失败')
    context = {
        'total': total,
        'on_line_count': on_line_count,
        'off_line_count': off_line_count
    }
    return render(request, 'message/dashboard.html', context)


def register(request):
    if request.method == 'GET':
        return render(request, 'message/register.html')
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        email = request.POST.get('email')
        user = User.objects.filter(username=username)
        email = '123@123.com'
        if not user:
            user = User.objects.create_user(username, email, password)
            context = {
                'user': user
            }
            return render(request, 'message/index.html', context)
        else:
            return HttpResponse('用户已经注册了')


def manager(request):
    id = request.GET.get('id')
    device = Device.objects.get(id=id)
    context = {
        'device': device,
    }
    return render(request, 'message/manager.html', context)








