# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Device',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('mac', models.CharField(max_length=64)),
                ('name', models.CharField(max_length=64)),
                ('type', models.CharField(max_length=64)),
                ('Firmware', models.CharField(max_length=64)),
                ('note', models.CharField(max_length=64)),
                ('active_time', models.DateField()),
                ('version', models.CharField(max_length=64)),
                ('online_time', models.DateField()),
                ('offline_time', models.DateField()),
                ('user_name', models.CharField(max_length=64)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(max_length=64)),
            ],
        ),
    ]
