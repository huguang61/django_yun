from django.shortcuts import render, redirect
from django.contrib.auth.models import User, Permission, Group
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType


def index(request):
    return HttpResponse('这是首页')


def register(request):
    if request.method == 'GET':
        return render(request, 'authority/register.html')
    else:
        username = request.POST.get('username')
        password = request.POST.get('password')
        email = '123@123.com'
        try:
            user = User.objects.create_user(username, email, password)
        except Exception as e:
            return HttpResponse('注册用户失败')
        user.is_active = 1
        user.is_staff = 1
        user.save()
        return redirect('/authority/add_user_detail/')


def loginview(request):
    if request.method == 'GET':
        return render(request, 'authority/login.html')
    else:
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user is None:
            return HttpResponse('用户名或密码错误')
        else:
            login(request, user)
            return redirect('/authority/myadmin')


@login_required()
def logoutview(request):
    logout(request)
    return redirect('/authority/loginview/')


@login_required()
def myadmin(request):
    user = request.user
    models = ContentType.objects.filter(app_label='message').all()
    list1 = []
    list2 = []
    model_list = []
    for model in models:
        model_list.append(model)
    device = model_list[0]
    team = model_list[1]
    content_type = ContentType.objects.get(id=device.id)
    model_permissions = content_type.permission_set.all()
    list = []
    for permission in model_permissions:
        list.append(permission.id)
    permissions = user.user_permissions.filter(id__in=list)
    for permission in permissions:
        list1.append(permission.id)

    content_type = ContentType.objects.get(id=team.id)
    model_permissions = content_type.permission_set.all()
    list = []
    for permission in model_permissions:
        list.append(permission.id)
    permissions = user.user_permissions.filter(id__in=list)
    for permission in permissions:
        list2.append(permission.id)
    context = {
        'device': device,
        'team': team,
        'list1': list1,
        'list2': list2,
    }
    return render(request, 'authority/myadmin.html', context)


@login_required()
def user_list(request):
    users = User.objects.all()
    context = {
        'users': users
    }
    return render(request, 'authority/user_list.html', context)


@login_required()
def add_user(request):
    if request.method == 'GET':
        return render(request, 'authority/add_user.html')
    else:
        username = request.POST.get('username')
        password = request.POST.get('password1')
        password2 = request.POST.get('password2')
        email = request.POST.get('email')
        if not all([username, password, password2]):
            return HttpResponse('请将信息输入完整')
        if password == password2:
            try:
                user1 = User.objects.create_user(username, email, password)
                user1.save()
            except Exception as e:
                return HttpResponse('创建用户失败')

            user = request.user
            if user.is_superuser:
                groups = Group.objects.all()
                permissions = Permission.objects.all()
                context = {
                    'user1': user1,
                    'groups': groups,
                    'permissions': permissions
                }
            else:
                permissions = user.user_permissions.all()
                groups = user.groups.all()
                context = {
                    'permissions': permissions,
                    'groups': groups,
                    'user1': user1,
                }
            return render(request, 'authority/add_user_datail.html', context)
        else:
            return HttpResponse('两次密码输入不一致')


@login_required()
def add_user_detail(request):
    if request.method == 'GET':
        permissions = Permission.objects.all()
        groups = Group.objects.all()
        context = {
            'permissions': permissions,
            'groups': groups
        }
        return render(request, 'authority/add_user_datail.html', context)
    else:
        username = request.POST.get('username')
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        is_active = request.POST.get('is_active')
        is_staff = request.POST.get('is_staff')
        is_superuser = request.POST.get('is_superuser')
        print(is_superuser, 3)

        try:
            user = User.objects.get(username=username)
        except Exception as e:
            return HttpResponse('查询数据库失败')

        user.first_name = first_name
        user.username = username
        user.last_name = last_name
        if is_active == 'on':
            user.is_active = 1
        else:
            user.is_active = 0
        if is_staff == 'on':
            user.is_staff = 1
        else:
            user.is_staff = 0
        if is_superuser == 'on':
            user.is_superuser = 1
        else:
            user.is_superuser = 0

        groups = Group.objects.all()
        mouth = 1
        for group in groups:
            if group.id > mouth:
                mouth = group.id
        a = 1
        group_list1 = []
        while a <= mouth:
            id = request.POST.get('group%d' % a)
            if id is not None:
                group_list1.append(id)
            a += 1
        delete_groups = user.groups.all()
        for group in delete_groups:
            user.groups.remove(group)
        for group in group_list1:
            user.groups.add(group)

        permissions = Permission.objects.all()
        count = 1
        for permission in permissions:
            if permission.id > count:
                count = permission.id
        i = 1
        permission_list = []
        while i <= count:
            id = request.POST.get('permission%d' % i)
            if id is not None:
                permission_list.append(id)
            i += 1
        delete_permissions = user.user_permissions.all()
        for permission in delete_permissions:
            user.user_permissions.remove(permission)
        for permission in permission_list:
            user.user_permissions.add(permission)
        user.save()
        return redirect('/authority/myadmin/')


@login_required()
def change_pwd(request):
    if request.method == 'GET':
        return render(request, 'authority/change_pwd.html')
    else:
        old_password = request.POST.get('old_password')
        new_password1 = request.POST.get('new_password1')
        new_password2 = request.POST.get('new_password2')
        user = request.user
        user1 = authenticate(username=user.username, password=old_password)

        if not user1:
            return HttpResponse('密码输入有误')
        if not new_password1 == new_password2:
            return HttpResponse('两次输入密码不相同')

        user1.set_password(new_password1)
        user1.save()

        return redirect('/authority/myadmin/')


@login_required()
def group_list(request):

    groups = Group.objects.all()
    count = len(groups)
    context = {
        'groups': groups,
        'count': count
    }

    return render(request, 'authority/group_list.html', context)


@login_required()
def add_group(request):
    if request.method == 'GET':
        permissions = Permission.objects.all()
        context = {
            'permissions': permissions
        }
        return render(request, 'authority/add_group.html', context)
    else:
        group_name = request.POST.get('name')
        if not group_name:
            return HttpResponse('请输入组名')
        group = Group.objects.create(name=group_name)
        if group is None:
            return HttpResponse('用户组创建失败')
        permissions = Permission.objects.all()
        count = len(permissions)
        i = 1
        permission_list = []
        while i <= count:
            id = request.POST.get('permission%d' % i)
            if id is not None:
                permission_list.append(id)
            i += 1
        for permission in permission_list:
            group.permissions.add(permission)
        group.save()
        return redirect('/authority/group_list/')


@login_required()
def delete_group(request):
    groups = Group.objects.all()
    mouth = 1
    for group in groups:
        if group.id > mouth:
            mouth = group.id
    a = 1
    group_list1 = []
    while a <= mouth:
        id = request.POST.get('group%d' % a)
        if id is not None:
            group_list1.append(id)
        a += 1

    for idd in group_list1:
        groups = Group.objects.filter(id=idd)

        for group in groups:
            group.permissions.clear()
            group.user_set.clear()
            group.delete()

    return redirect('/authority/group_list/')


@login_required()
def delete_user(request):
    users = User.objects.all()
    mouth = 1
    for user in users:
        if user.id > mouth:
            mouth = user.id
    a = 0
    user_list1 = []
    while a <= mouth:
        id = request.POST.get('user%d' % a)
        if id is not None:
            user_list1.append(id)
        a += 1

    for idd in user_list1:
        users = User.objects.filter(id=idd)
        for user in users:
            user.user_permissions.clear()
            user.groups.clear()
            user.delete()

    return redirect('/authority/user_list/')


@login_required()
def users_detail(request, page):
    permissions = Permission.objects.all()
    groups = Group.objects.all()
    user = User.objects.get(id=page)
    user1_permissions = user.user_permissions.all()
    a = 1
    list1 = []
    for user1_permission in user1_permissions:
        list1.append(user1_permission.id)
    list_group = []
    user1_groups = user.groups.all()
    for user1_group in user1_groups:
        list_group.append(user1_group.id)
    context = {
        'user1': user,
        'permissions': permissions,
        'groups': groups,
        'list1': list1,
        'list_group': list_group,
    }
    return render(request, 'authority/add_user_datail.html', context)


@login_required()
def groups_detail(request, page):
    permissions = Permission.objects.all()
    group = Group.objects.get(id=page)
    group.permission = group.permissions.all()
    a = 1
    list1 = []
    for permission in group.permission:
        list1.append(permission.id)
    context = {
        'group': group,
        'permissions': permissions,
        'list1': list1,
    }
    return render(request, 'authority/change_group_permission.html', context)


@login_required()
def change_group_permission(request, page):
    group = Group.objects.get(id=page)
    group.permission = group.permissions.all()
    a = 1
    list1 = []
    for permission in group.permission:
        list1.append(permission.id)
    permissions = Permission.objects.all()
    count = len(permissions)
    i = 1
    permission_list = []
    while i <= count:
        id = request.POST.get('permission%d' % i)
        if id is not None:
            permission_list.append(id)
        i += 1
    delete_permissions = group.permissions.all()
    for permission in delete_permissions:
        group.permissions.remove(permission)
    for permission in permission_list:
        group.permissions.add(permission)
    group.save()
    return redirect('/authority/group_list/')


@login_required()
def show_devices(request, id):
    user = request.user
    content_type = ContentType.objects.get(id=id)
    model_permissions = content_type.permission_set.all()
    list = []
    for permission in model_permissions:
        list.append(permission.id)
    permissions = user.user_permissions.filter(id__in=list)
    print(permissions)
    return HttpResponse('成功')















