from django.template import Library
register = Library()

@register.filter
def mod(value, list1):
    if value in list1:
        return True
    else:
        return False


@register.filter
def primary(value):
    if len(value):
        return True
    else:
        return False