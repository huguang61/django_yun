from django.conf.urls import url
from authority import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^register/$', views.register),
    url(r'^loginview/$', views.loginview),
    url(r'^myadmin/$', views.myadmin),
    url(r'^logoutview/$', views.logoutview),
    url(r'^user_list/$', views.user_list),
    url(r'^add_user/$', views.add_user),
    url(r'^add_user_detail/$', views.add_user_detail),
    url(r'^change_pwd/$', views.change_pwd),
    url(r'^group_list/$', views.group_list),
    url(r'^add_group/$', views.add_group),
    url(r'^delete_group/$',views.delete_group),
    url(r'^delete_user/$', views.delete_user),
    url(r'^register1/$', views.register),
    url(r'^add_user_details/(\d+)/', views.users_detail),
    url(r'^groups_detail/(\d+)/', views.groups_detail),
    url(r'^change_group_permission/(\d+)/', views.change_group_permission),
    url(r'^show_devices/(\d+)/', views.show_devices),
]






















